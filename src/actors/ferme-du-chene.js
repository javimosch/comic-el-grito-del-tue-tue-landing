module.exports = {
	metaTitle: "Ferme du Chêne",
	name: "Ferme du Chêne",
	publicUrl: "/ferme-du-chene",
	activity: 'Production de produits laitiers frais',
	description: `Ferme à taille humaine, Respect de l’environnement et des animaux, Ingrédients naturels et souvent biologiques, lait cru (sauf pour les yaourts pasteurisés). Production de produits laitiers frais : lait, yaourts, petit frais, faisselle, fromage blanc`,
	address: {
		formatted: '73340 Le Noyer, Francia',
		coords: []
	},
	email: 'lafermeduchene73@gmail.com',
	phone: '0479342352',
	phones: [{
		name: "Magali",
		number: "06 72 12 08 86"
	}, {
		name: "Romain",
		number: "06 69 03 62 11"
	}],
	web: 'https://lafermeduchene.fr',
	owner: 'Magali Kriegk & Romain Gobé',
	sellPoints: [{
		address: `marché de Lescheraines (juillet et aout) dimanche matin`,
		when: 'juillet et out, dimaches',
		time: 'matin'
	}, {
		address: `Fruitière de Lescheraines`
	}, {
		address: `Carrefour contact le Châtelard`
	}],
	schedule: `A la ferme du lundi au samedi 17h-19h`,
	type: 'bon'
};
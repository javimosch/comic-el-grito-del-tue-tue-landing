module.exports = {
	metaTitle:"Croc'Bauges - Marche Bio et Locaux",
	name: "Croc'Bauges",
	publicUrl:"/crocbauges",
	activity:'épicerie bio associative',
	description: `produits Bio et Locaux, consommateurs peuvent être acteurs`,
	address: {
		formatted: 'Lescheraines le Pont',
		coords: []
	},
	email: '',
	phone: '0479342352',
	type:'bon'
};
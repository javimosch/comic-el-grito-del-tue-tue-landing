module.exports = {
	metaTitle:'Les Bau’jardins - Maraîchés en Agriculture Biologique',
	name: 'Les Bau’jardins',
	activity:'Maraîchés en Agriculture Biologique',
	description: ` Inspiré des recherches en agroforesterie et en Maraîchage sur Sol Vivant, l’objectif est de proposer à la vente des légumes naturels en prenant soin de la Terre et des Hommes.`,
	address: {
		formatted: 'ZA les îles 73630 La Compôte',
		coords: []
	},
	publicUrl:'/lesbaujardins',
	differentiation:"",
	email: '',
	phone: '0479342352',
	type:'bon'
};